<?php

namespace App\Console\Commands;

use App\Service\TokenService;
use Illuminate\Console\Command;

class SignTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'token:sign
                            {--token= : 要解的token}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var TokenService
     */
    protected $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TokenService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $token = $this->option('token');

        if (null !== $token) {
            $this->parse($token);
            return;
        }

        $this->sign();
    }

    protected function sign()
    {
        $data = [
            'foo' => 'bar',
        ];

        $token = $this->service->issue($data);

        $this->info($token);
    }

    protected function parse(string $token)
    {
        dump($this->service->parse($token));
    }
}
