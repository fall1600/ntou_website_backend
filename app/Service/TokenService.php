<?php

namespace App\Service;

use Carbon\Carbon;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Hmac\Sha384;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Token;

class TokenService
{
    /** @var string $secret */
    protected $secret;

    /** @var string $issuer */
    protected $issuer;

    /** @var Sha256 */
    protected $signer;

    /** @var Parser */
    protected $parser;

    public function __construct()
    {
        $this->signer = new Sha256();
        $this->parser = new Parser();

        $this->secret = env('JWT_SECRET');
        $this->issuer = env('JWT_ISSUER');
    }

    public function issue($payload)
    {
        $time = time();
        $token = (new Builder())->issuedBy($this->issuer) // Configures the issuer (iss claim)
//        ->permittedFor('http://example.org') // Configures the audience (aud claim)
//        ->identifiedBy('4f1g23a12aa', true) // Configures the id (jti claim), replicating as a header item
        ->issuedAt($time) // Configures the time that the token was issue (iat claim)
//        ->canOnlyBeUsedAfter($time + 60) // Configures the time that the token can be used (nbf claim)
        ->expiresAt($time + 3600) // Configures the expiration time of the token (exp claim)
        ->withClaim('data', $payload) // Configures a new claim, called "uid"
        ->getToken($this->signer, new Key($this->secret)); // Retrieves the generated token

        return $token;
    }

    public function parse(string $token): ?Token
    {
        $token = $this->parser->parse($token);

        if ($token->getHeader('alg') === "SHA384") {
            $this->signer = new Sha384();
        }

        if ($token->verify($this->signer, $this->secret) && !$token->isExpired(Carbon::now())) {
            return $token;
        }
        return null;
    }
}
