<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Project;
use App\Service\TokenService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends Controller
{
    public function index(Request $request, TokenService $tokenService)
    {
        return Project::limit(10)->get();
    }

    public function store(ProjectRequest $request)
    {
        $project = new Project();
        $project->title = $request->get('title');
        $project->content = $request->get('content');
        $project->save();
        return $project->id;
    }

    public function show(Project $project)
    {
        return response()->json($project);
    }

    public function update(ProjectRequest $request, Project $project)
    {
        $project->title = $request->get('title', $project->title);
        $project->content = $request->get('content', $project->content);
        $project->save();
        return $project->id;
    }

    public function destroy(Project $project)
    {
        if (!$project->delete()) {
            return response()->json("", Response::HTTP_BAD_REQUEST);
        }
        return response()->json();
    }
}
