<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Service\TokenService;
use App\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if(is_null($user)) {
            return response()->json("", Response::HTTP_NOT_FOUND);
        }

        if(!Hash::check($request->get('password'), $user->password)) {
            return response()->json("", Response::HTTP_BAD_REQUEST);
        }

        /** @var TokenService $tokenService */
        $tokenService = app(TokenService::class);
        $token = $tokenService->issue($user->id);

        return response()->json((string) $token);
    }

    public function register(RegisterRequest $request)
    {
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $hashed = Hash::make($request->get('password'), [
            'memory' => 1024,
            'time' => 2,
            'threads' => 2,
        ]);
        $user->password = $hashed;
        $user->save();

        return response()->json($user);
    }
}
