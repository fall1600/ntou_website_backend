<?php

namespace App\Http\Middleware;

use App\Service\TokenService;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class JwtTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $strToken = $request->headers->get('bearer-token');

        if (is_null($strToken)) {
            return response()->json("", Response::HTTP_UNAUTHORIZED);
        }

        /** @var TokenService $tokenService */
        $tokenService = app(TokenService::class);
        $token = $tokenService->parse($strToken);

        if (null === $token) {
            return response()->json("", Response::HTTP_UNAUTHORIZED);
        }

        $request->attributes->set('_authorizedToken', $token);
        return $next($request);
    }
}
