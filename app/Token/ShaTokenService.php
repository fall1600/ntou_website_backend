<?php

namespace App\Token;

use App\Token\TokenRequest\TokenRequestInterface;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class ShaTokenService extends AbstractTokenService
{
    protected $secret;

    protected $issuer;

    public function setSecret(string $secret)
    {
        $this->secret = $secret;
        return $this;
    }

    public function setIssuer(string $issuer)
    {
        $this->issuer = $issuer;
        return $this;
    }

    public function sign(TokenRequestInterface $tokenRequest): string
    {
        $signer = new Sha256();
        return (string) $this->applyData($this->issuer, $tokenRequest)
            ->getToken($signer, $this->secret);
    }

    public function parse(string $jwtToken)
    {

    }
}