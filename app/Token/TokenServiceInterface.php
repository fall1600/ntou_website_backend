<?php

namespace App\Token;

use App\Token\TokenRequest\TokenRequestInterface;

interface TokenServiceInterface
{
    public function sign(TokenRequestInterface $tokenRequest): string;
    public function parse(string $jwtToken);
}
