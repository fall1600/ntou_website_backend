<?php

namespace App\Token;

use App\Token\TokenRequest\TokenRequestInterface;
use Lcobucci\JWT\Builder;

abstract class AbstractTokenService implements TokenServiceInterface
{
    protected function getCurrentTime()
    {
        return time();
    }

    /**
     * @param string $issuer
     * @param TokenRequestInterface $tokenRequest
     * @return Builder
     */
    protected function applyData(string $issuer, TokenRequestInterface $tokenRequest)
    {
        $time = $this->getCurrentTime();
        return (new Builder())
            ->issuedBy($issuer)
            ->issuedAt($time)
            ->expiresAt($time + $tokenRequest->getTtl())
            ->withClaim('id', $tokenRequest->getId())
            ->withClaim('type', $tokenRequest->getType())
            ->withClaim('data', $tokenRequest->getData());
    }
}