<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::middleware('jwt.api')->post('projects', 'ProjectController@create');
//Route::middleware('jwt.api')->get('project/{project}', 'ProjectController@read');
//Route::middleware('jwt.api')->put('project/{project}', 'ProjectController@update');
//Route::middleware('jwt.api')->delete('project/{project}', 'ProjectController@delete');
Route::middleware('jwt.api')->resource('project', 'ProjectController', ['only' => [
    'index', 'store', 'show', 'update', 'destroy'
]]);

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
