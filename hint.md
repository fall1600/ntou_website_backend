```shell script
# 建專案
$ composer create-project --prefer-dist laravel/laravel {project-name}

# 開出哪些api
$ php artisan route:list

# 註冊
$ http POST localhost:8000/api/register email=fall1600@gmail.com name=boo password=5566

# 登入 (取得token)
$ http POST localhost:8000/api/login email=fall1600@gmail.com password=5566

# 建 project model
$ http POST localhost:8000/api/project title=title content=bar Bearer-Token:{token}

# 取得 project model
$ http GET localhost:8000/api/project Bearer-Token:{token}

# 取得單一 project model
$ http GET localhost:8000/api/project/2 Bearer-Token:{token}

# 編輯單一 project model
$ http PUT localhost:8000/api/project/2 Bearer-Token:{token}

```

